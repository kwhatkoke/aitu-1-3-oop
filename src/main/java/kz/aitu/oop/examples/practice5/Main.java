package kz.aitu.oop.examples.practice5;

import java.io.FileNotFoundException;

public class Main {
    Main(String str) {
        str = "It must be done!";
        System.out.println(str);
    }

    public static void main(String[] args) throws Exception {
        try {
            throw new FileNotFoundException();
        } catch (FileNotFoundException e) {
            throw new Exception("File not found");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("Get printed");
        }
    }
}
