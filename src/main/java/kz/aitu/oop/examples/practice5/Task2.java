package kz.aitu.oop.examples.practice5;

import java.io.File;

public class Task2 {

    public static boolean fileExists(String path){
        try{
            File f = new File(path);
            if(f.exists()){
                return true;
            }
        }catch(Exception e){
        }
        return false;
    }

    public static boolean isInt(String input){
        try{
            Integer.parseInt(input);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    public static boolean isDouble(String input){
        try{
            Double.parseDouble(input);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    public static void main(String[] args) {
        String path = "C:\\Users\\Kwhat\\Desktop\\file2.txt";
        String input1 = "7";
        String input2 = "7.7";
        System.out.println(fileExists(path));
        System.out.println("-------------");
        System.out.println(isInt(input1));
        System.out.println("-------------");
        System.out.println(isDouble(input2));
    }
}
