package kz.aitu.oop.examples.practice6;

public class Singleton {
    public static String str;
    private static Singleton instance = null;

    private Singleton() {

    }

    static Singleton getSingleInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}
