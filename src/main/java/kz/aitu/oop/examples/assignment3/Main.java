package kz.aitu.oop.examples.assignment3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int[] arr = {7,6,1,2,4,10,3,8,9,3};
        MyString object = new MyString(arr);
        System.out.println(object.length());
        System.out.println(object.contains(4));
        System.out.println(object.valueAt(4));
        System.out.println(object.count(6));
        object.print();

        System.out.println("Please write name of your file:");
        String name = sc.nextLine();
        String path = "C:\\Users\\Kwhat\\Desktop\\" + name + ".txt"; //name = assignment3
        File file = new File(path);
        FileWriter writer = new FileWriter(file);
        for (int i = 0; i < object.length(); i++){
            writer.write(object.valueAt(i) + " ");
        }
        writer.close();

        Scanner sc2 = new Scanner(file);
        Scanner sizesc = new Scanner(file);
        int s = 0;
        while(sizesc.hasNextInt()){
            System.out.print(sizesc.nextInt() + " ");
            s++;
        }
        System.out.println();

        int[] arr2 = new int[s];
        int j = 0;
        while(sc2.hasNextInt()){
            arr2[j] = sc2.nextInt();
            j++;
        }

        MyString object2 = new MyString(arr2);
        object2.print();

        MySpecialString object3 = new MySpecialString(arr);
        System.out.println(object3.length());
        System.out.println(object3.contains(7));
        System.out.println(object3.valueAt(3));
        System.out.println(object3.count(4));
        object.print();
        String[] arrayS = {"Kuat", "Kuat", "Baska", "Bireu"};
        NewSpecialString objectStr = new NewSpecialString(arrayS);
        System.out.println(objectStr.length());
        System.out.println(objectStr.contains("Kuat"));
        System.out.println(objectStr.valueAt(3));
        System.out.println(objectStr.valueAt(-1));
        System.out.println(objectStr.count("Kuat"));
        objectStr.print();
    }
}
