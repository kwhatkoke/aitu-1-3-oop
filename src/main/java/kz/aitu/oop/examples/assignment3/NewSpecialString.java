package kz.aitu.oop.examples.assignment3;

public class NewSpecialString {
    private String[] arr;

    public NewSpecialString(String[] array){
        this.arr=array;
    }

    public int length(){
        return arr.length;
    }

    public String valueAt(int position){
        if(!(position>=0 && position<arr.length)){
            return "-1";
        }
        return arr[position];
    }

    public boolean contains(String value){
        for(int i=0; i < arr.length; i++){
            if(arr[i].equals(value)){
                return true;
            }
        }
        return false;
    }

    public int count(String value){
        int c = 0;
        for(int i=0; i < arr.length; i++){
            if(arr[i].equals(value)){
                c++;
            }
        }
        return c;
    }

    public void print(){
        for(int i=0; i < arr.length; i++){
            System.out.println(arr[i]);
        }
    }
}
