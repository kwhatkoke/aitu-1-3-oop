package kz.aitu.oop.examples.assignment3;

import java.util.HashSet;

public class MySpecialString {
    int[] arr;

    public MySpecialString(int[] array){
        this.arr=array;
    }

    public int length(){
        return arr.length;
    }


    public int valueAt(int position){
        if(arr[position] == '\0'){
            return -1;
        }
        return arr[position];
    }

    public boolean contains(int value){
        for(int i=0; i < arr.length; i++){
            if(arr[i] == value){
                return true;
            }
        }
        return false;
    }

    public int count(int value){
        int c = 0;
        for(int i=0; i < arr.length; i++){
            if(arr[i] == value){
                c++;
            }
        }
        return c;
    }

    public void print(){
        for(int i=0; i < arr.length; i++){
            System.out.println(arr[i]);
        }
    }
}

