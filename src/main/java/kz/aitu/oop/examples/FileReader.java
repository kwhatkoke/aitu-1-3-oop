package kz.aitu.oop.examples;

// Java Program to illustrate reading from Text File
// using Scanner Class
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileReader {
    public static void main(String[] args) throws Exception {

        FileReader fileReader = new FileReader();
        System.out.println(fileReader.getFile());
    }

    public String getFile() throws FileNotFoundException {

        File file = new File("C:\\Users\\Kwhat\\Desktop\\file1.txt"); //for file2.txt path --> C:\Users\Kwhat\Desktop\file1.txt
        Scanner sc = new Scanner(file);
        String result = "";
        while (sc.hasNextLine())
            result += sc.nextLine() + "\n";
        String temp="";

        List<Integer> arr = new ArrayList<>();
        int a=0;

        for (int i = 0;i<result.length();i++){
            if (result.charAt(i)==',' || result.charAt(i)=='\n'){
                arr.add(a, Integer.parseInt(temp));
                a++;
                temp="";
            } else {
                if (result.charAt(i)!=' ') {
                    temp += result.charAt(i);
                }
            }
        }

        ArrayList<Point> points = new ArrayList<>();
        for (int i=0;i<arr.size();i+=2){
            double x = arr.get(i);
            double y = arr.get(i+1);
            Point p1 = new Point(x,y);
            points.add(p1);
        }

        Point p1 = new Point(5, 8);
        Point p2 = new Point(1, 3);
        double distance = p2.distance(p1);

        Shape shape = new Shape();
        shape.setPoints(points);
        String out = "";
        out+="Distance between two points: " + distance + "<br>";
        out+="Points: " + result + "<br>";
        out+="Perimeter of the shape is: " + shape.calculatePerimeter()+"<br>";
        out+="Longest side is: " + shape.longest()+"<br>";
        out+="Average length is: " + shape.average();
        return out;
    }
}
