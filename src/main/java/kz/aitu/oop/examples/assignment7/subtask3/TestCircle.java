package kz.aitu.oop.examples.assignment7.subtask3;

public class TestCircle {
    public static void main(String[] args) {
        Circle circle = new Circle(7);
        System.out.println(circle.getArea());
        System.out.println(circle.getPerimeter());
    }
}
