package kz.aitu.oop.examples.assignment7.subtask3;

public class ResizableCircle extends Circle implements Resizable  {

    public ResizableCircle(int radius){
       super(radius);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void resize(int percent) {
        System.out.println(percent);
    }
}
