package kz.aitu.oop.examples.assignment7.subtask3;

public class TestResizableCircle {
    public static void main(String[] args) {
        ResizableCircle resizableCircle = new ResizableCircle(10);
        System.out.println(resizableCircle.toString());
        resizableCircle.resize(5);
    }
}
