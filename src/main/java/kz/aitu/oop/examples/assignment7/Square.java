package kz.aitu.oop.examples.assignment7;

public class Square extends Rectangle {

    public Square(){
        super();
    }

    public Square(double side){
        super(side, side);
    }

    public Square(double side, String color, boolean filled){
        super(side, side, color, filled);
    }

    public void setSide(double side) {
        super.setWidth(side);
    }

    public double getSide() {
       return super.getWidth();
    }

    @Override
    public String toString() {
        return "A Square with side="+super.getWidth()+", which is subclass of "+super.toString();
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
    }
}
