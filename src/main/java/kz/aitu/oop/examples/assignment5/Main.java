package kz.aitu.oop.examples.assignment5;

import java.io.IOException;


public class Main {

    public static void main(String[] args) throws IOException {
        Shape shape = new Shape("red", true);
        System.out.println(shape.getColor());
        System.out.println(shape.isFilled());
        System.out.println(shape.toString());
        Circle circle = new Circle(7.7);
        System.out.println(circle.getArea());
        System.out.println(circle.getPerimeter());
        System.out.println(circle.getColor());
        System.out.println(circle.toString());
        Rectangle rectangle = new Rectangle(6.9, 11.0);
        System.out.println(rectangle.getArea());
        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle.toString());
        Square square = new Square(7);
        System.out.println(square.toString());
        System.out.println(square.getArea());
        System.out.println(square.getPerimeter());
    }
}
