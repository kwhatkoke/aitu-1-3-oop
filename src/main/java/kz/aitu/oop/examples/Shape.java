package kz.aitu.oop.examples;

import java.util.List;

public class Shape {
    private List<Point> points;

    public Shape(){

    }

    public void addPoint(Point point){
        this.points.add(point);
    }

    public void setPoints(List<Point> points1) {
        points = points1;
    }

    public Point getPoints(int point_index) {
        return points.get(point_index);
    }

    public Shape(List<Point> points){
        this.points = points;
    }

    public double calculatePerimeter() {
        double perimeter=0;
        for (int i=0;i<points.size()-1;i++){
            Point p1 = points.get(i);
            Point p2 = points.get(i+1);
            perimeter+=p1.distance(p2);
        }
        Point start = points.get(0);
        Point end = points.get(points.size()-1);
        perimeter+=start.distance(end);
        return perimeter;

    }

    public double longest(){
        double c = 0;
        Point a = points.get(0);
        Point b = points.get(1);
        double longest =0;
        longest += a.distance(b);
        for (int i=0;i<points.size()-1;i++){
            Point p1 = points.get(i);
            Point p2 = points.get(i+1);
            c=p1.distance(p2);
            if(longest<c){
                longest = c;
            }
        }
        return longest;

    }

    public double average(){
        double ave = 0;
        for (int i=0;i<points.size()-1;i++){
            Point p1 = points.get(i);
            Point p2 = points.get(i+1);
            ave +=p1.distance(p2);
        }
        Point start = points.get(0);
        Point end = points.get(points.size()-1);
        ave += start.distance(end);
        ave = ave / points.size()+1;
        return ave;
    }

}

