package kz.aitu.oop.examples.practice7;

public class FoodFactory {

    public Food getFood(String zakaz) {
        switch (zakaz) {
            case "pizza":
                return new Pizza();
            case "cake":
                return new Cake();
            default:
                return null;
        }
    }
}
