package kz.aitu.oop.practice_with_patterns.practice4;

public class FishBuilder {

    public String name;
    public double cost;
    public double size;

    public FishBuilder addFishName(String name){
        this.name = name;
        return this;
    }

    public FishBuilder addFishCost(double cost){
        this.cost = cost;
        return this;
    }

    public FishBuilder addFishSize(double size){
        this.size = size;
        return this;
    }

    public Fish make(){
        return new Fish(this);
    }
}
