package kz.aitu.oop.practice_with_patterns.practice4;

public class ReptileBuilder {

    public String name;
    public double cost;
    public double size;

    public ReptileBuilder addReptileName(String name){
        this.name = name;
        return this;
    }

    public ReptileBuilder addReptileCost(double cost){
        this.cost = cost;
        return this;
    }

    public ReptileBuilder addReptileSize(double size){
        this.size = size;
        return this;
    }

    public Reptile make(){
        return new Reptile(this);
    }
}
