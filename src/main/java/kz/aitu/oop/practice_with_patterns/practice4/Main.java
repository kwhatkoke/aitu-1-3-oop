package kz.aitu.oop.practice_with_patterns.practice4;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final String url = "jdbc:mysql://localhost/practice?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String user = "root";
    private static final String password = "123456";

    public static Connection connect() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    public void insertToAquarium(List<Aquarium> list) {
        String SQL = "INSERT INTO aquarium(type, name, cost, size) " + "VALUES(?,?,?,?)";
        try (
                Connection conn = connect();
                PreparedStatement statement = conn.prepareStatement(SQL);) {
            int count = 0;

            for (Aquarium aquarium : list) {
                statement.setString(1, aquarium.getType());
                statement.setString(2, aquarium.getName());
                statement.setDouble(3, aquarium.getCost());
                statement.setDouble(4, aquarium.getSize());
                statement.addBatch();
                count++;
                // execute every 100 rows or less
                if (count % 100 == 0 || count == list.size()) {
                    statement.executeBatch();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        ArrayList<Aquarium> aquariums = new ArrayList<>();
        double total_cost = 0;
        FishBuilder fishBuilder = new FishBuilder();
        Fish fish1 = fishBuilder.
                addFishName("Catfish").
                addFishCost(7000).
                addFishSize(20).
                make();
        Fish fish2 = fishBuilder.
                addFishName("Oskar").
                addFishCost(10000).
                addFishSize(9).
                make();
        ReptileBuilder reptileBuilder = new ReptileBuilder();
        Reptile reptile1 = reptileBuilder.
                addReptileName("Turtle").
                addReptileCost(6780).
                addReptileSize(21).
                make();
        Reptile reptile2 = reptileBuilder.
                addReptileName("Python").
                addReptileCost(8000).
                addReptileSize(19).
                make();
        aquariums.add(new Fish(fish1.getType(), fish1.getName(), fish1.getCost(), fish1.getSize()));
        aquariums.add(new Fish(fish2.getType(), fish2.getName(), fish2.getCost(), fish2.getSize()));
        aquariums.add(new Reptile(reptile1.getType(), reptile1.getName(), reptile1.getCost(), reptile1.getSize()));
        aquariums.add(new Reptile(reptile2.getType(), reptile2.getName(), reptile2.getCost(), reptile2.getSize()));
        main.insertToAquarium(aquariums);

        try{
            Connection conn = connect();
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery("select * from aquarium");
            while(rs.next()){
                System.out.println(rs.getInt("id") + " "
                        + rs.getString("type") + " "
                        + rs.getString("name") + " "
                        + rs.getDouble("cost") + " "
                        + rs.getDouble("size"));
                total_cost += rs.getDouble(5);
            }
            System.out.println("The total cost: " + total_cost);
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
