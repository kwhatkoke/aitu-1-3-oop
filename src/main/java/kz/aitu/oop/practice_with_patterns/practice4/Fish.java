package kz.aitu.oop.practice_with_patterns.practice4;

public class Fish extends Aquarium{

    private String type;
    private String name;
    private double cost;
    private double size;

    public Fish(FishBuilder fishBuilder){
        this.type = "Fish";
        this.name = fishBuilder.name;
        this.cost = fishBuilder.cost;
        this.size = fishBuilder.size;
    }

    public Fish(String type, String name, double size, double cost) {
        this.type = type;
        this.name = name;
        this.size = size;
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public double getSize() {
        return size;
    }

}

