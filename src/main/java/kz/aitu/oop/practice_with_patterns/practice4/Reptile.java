package kz.aitu.oop.practice_with_patterns.practice4;

public class Reptile extends Aquarium {

    private String type;
    private String name;
    private double cost;
    private double size;

    public Reptile(ReptileBuilder reptileBuilder){
        this.type = "Reptile";
        this.name = reptileBuilder.name;
        this.cost = reptileBuilder.cost;
        this.size = reptileBuilder.size;
    }

    public Reptile(String type, String name, double size, double cost) {
        this.type = type;
        this.name = name;
        this.size = size;
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }
}
