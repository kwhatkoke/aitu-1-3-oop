package kz.aitu.oop.practice_with_patterns.practice2;

public class Locomotive extends Train {

    private int number_of_drivers;

    public Locomotive(double capacity, double price, double passengers, int number_of_drivers) {
        super(capacity, price, passengers);
        this.number_of_drivers = number_of_drivers;
    }

    public int getNumber_of_drivers() {
        return number_of_drivers;
    }

    public void setNumber_of_drivers(int number_of_drivers) {
        this.number_of_drivers = number_of_drivers;
    }
}
