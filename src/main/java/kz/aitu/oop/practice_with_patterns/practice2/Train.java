package kz.aitu.oop.practice_with_patterns.practice2;


public class Train {
    private double capacity;
    private double price;
    private double passengers;

    public Train(double capacity, double price, double passengers) {
        this.capacity = capacity;
        this.price = price;
        this.passengers = passengers;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPassengers() {
        return passengers;
    }

    public void setPassengers(double passengers) {
        this.passengers = passengers;
    }
}
