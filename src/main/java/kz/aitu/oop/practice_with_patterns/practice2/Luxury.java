package kz.aitu.oop.practice_with_patterns.practice2;

public class Luxury extends Carriege {


    public Luxury(int passengers, int number_of_carrieges, int capacity) {
        super(passengers, number_of_carrieges, capacity);
    }

    @Override
    public double getCapacity() {
        return super.getCapacity();
    }

    @Override
    public void setCapacity(double capacity) {
        super.setCapacity(capacity);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
