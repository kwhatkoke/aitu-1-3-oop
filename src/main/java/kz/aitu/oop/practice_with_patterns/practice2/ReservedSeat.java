package kz.aitu.oop.practice_with_patterns.practice2;

public class ReservedSeat extends Carriege {
    private double price;

    public ReservedSeat(int passengers, int number_of_carrieges, int capacity) {
        super(passengers, number_of_carrieges, capacity);
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public double getCapacity() {
        return super.getCapacity();
    }

    @Override
    public void setCapacity(double capacity) {
        super.setCapacity(capacity);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
