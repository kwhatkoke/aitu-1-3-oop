package kz.aitu.oop.practice_with_patterns.practice2;


import kz.aitu.oop.practice.practice3.Employer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final String url = "jdbc:mysql://localhost/practice?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String user = "root";
    private static final String password = "123456";

    public static Connection connect() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    public void insertTrain(List<Train> list) {
        String SQL = "INSERT INTO train(capacity, price, passengers) " + "VALUES(?,?,?)";
        try (
                Connection conn = connect();
                PreparedStatement statement = conn.prepareStatement(SQL);) {
            int count = 0;

            for (Train train : list) {
                statement.setDouble(1, train.getCapacity());
                statement.setDouble(2, train.getPrice());
                statement.setDouble(3, train.getPassengers());
                statement.addBatch();
                count++;
                // execute every 100 rows or less
                if (count % 100 == 0 || count == list.size()) {
                    statement.executeBatch();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        CarriegeBuilder carriegeBuilder = new CarriegeBuilder("Train1");
        Carriege carriege = carriegeBuilder.
                addCarriegeCapacity().
                addCarriegePassengers().
                addCarriegePrice().
                make();
        double total_capacity = 0;
        int total_passengers = 0;
        ArrayList<Train> trains = new ArrayList<>();
        trains.add(new Train(5000, 3000, 40));
        trains.add(new Train(3000, 1000, 60));
        trains.add(new Train(4000, 2000, 30));
        main.insertTrain(trains);

        try{
            Connection conn = connect();
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery("select * from train");
            while(rs.next()){
                System.out.println(rs.getInt("id") + " "
                        + rs.getDouble("capacity") + " "
                        + rs.getDouble("price") + " "
                        + rs.getInt("passengers"));
                total_capacity += rs.getDouble("capacity");
                total_passengers += rs.getInt("passengers");
            }
            System.out.println("The total capacity: " + total_capacity);
            System.out.println("The total amount of passengers: " + total_passengers);
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
