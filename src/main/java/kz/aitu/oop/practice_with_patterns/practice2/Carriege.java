package kz.aitu.oop.practice_with_patterns.practice2;

public class Carriege extends Train {
    private double price;
    private int passengers;
    private int number_of_carrieges;
    private int capacity;

    public Carriege(double capacity, double price, double passengers) {
        super(capacity, price, passengers);
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public double getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    public int getNumber_of_carrieges() {
        return number_of_carrieges;
    }

    public void setNumber_of_carrieges(int number_of_carrieges) {
        this.number_of_carrieges = number_of_carrieges;
    }

    @Override
    public void setCapacity(double capacity) {
        super.setCapacity(capacity);
    }

    @Override
    public double getCapacity() {
        return super.getCapacity();
    }

    @Override
    public String toString() {
        return "Carriege{" +
                "price=" + price +
                ", passengers=" + passengers +
                '}';
    }
}
