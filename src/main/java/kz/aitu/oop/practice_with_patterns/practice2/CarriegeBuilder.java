package kz.aitu.oop.practice_with_patterns.practice2;

public class CarriegeBuilder {

    private Carriege carriege;
    private Train train;

    public CarriegeBuilder(String name) {
        carriege = new Carriege(40, 1, 3000);
        train = new Train(5000, 4000, 60);
    }

    public CarriegeBuilder addCarriegePrice(){
        carriege.setPrice(3000);
        return this;
    }

    public CarriegeBuilder addCarriegePassengers(){
        carriege.setPassengers(40);
        return this;
    }

    public CarriegeBuilder addCarriegeCapacity(){
        carriege.setCapacity(5000);
        return this;
    }

    public Carriege make(){
        return carriege;
    }
}
