package kz.aitu.oop.practice_with_patterns.practice3;

public class Web_developer extends Employer {

    private String type = "Web Developer";

    public Web_developer(String name, int experience_years) {
        super(name, experience_years);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
