package kz.aitu.oop.practice_with_patterns.practice3;

public class ProjectBuilder {
    public String project_name;
    public double project_cost;

    public ProjectBuilder addProjectName(String project_name){
        this.project_name = project_name;
        return this;
    }

    public ProjectBuilder addProjectCost(double cost){
        this.project_cost = cost;
        return this;
    }

    public Project make(){
        return new Project(this);
    }

}
