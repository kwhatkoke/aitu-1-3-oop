package kz.aitu.oop.practice_with_patterns.practice3;

public class Employer {

    private String name;
    private int experience_years;
    private String type;

    public Employer(String name, int experience_years, String type) {
        this.name = name;
        this.experience_years = experience_years;
        this.type = type;
    }

    public Employer(String name, int experience_years) {
        this.name = name;
        this.experience_years = experience_years;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getExperience_years() {
        return experience_years;
    }

    public void setExperience_years(int experience_years) {
        this.experience_years = experience_years;
    }
}
