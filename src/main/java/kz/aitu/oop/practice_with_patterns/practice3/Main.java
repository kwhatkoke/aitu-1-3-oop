package kz.aitu.oop.practice_with_patterns.practice3;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final String url = "jdbc:mysql://localhost/practice?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String user = "root";
    private static final String password = "123456";

    public static Connection connect() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    public void insertEmployers(List<Employer> list) {
        String SQL = "INSERT INTO employers(name, experience_years, type) " + "VALUES(?,?,?)";
        try (
                Connection conn = connect();
                PreparedStatement statement = conn.prepareStatement(SQL);) {
            int count = 0;

            for (Employer employer : list) {
                statement.setString(1, employer.getName());
                statement.setInt(2, employer.getExperience_years());
                statement.setString(3, employer.getType());
                statement.addBatch();
                count++;
                // execute every 100 rows or less
                if (count % 100 == 0 || count == list.size()) {
                    statement.executeBatch();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        ProjectBuilder projectBuilder = new ProjectBuilder();
        Project sub_project1 = projectBuilder.addProjectName("Making website design").addProjectCost(500).make();
        Project sub_project2 = projectBuilder.addProjectName("Background of website").addProjectCost(1000).make();
        Project sub_project3 = projectBuilder.addProjectName("Working with Java").addProjectCost(800).make();
        Project sub_project4 = projectBuilder.addProjectName("Working with PHP").addProjectCost(700).make();
        double total_cost = 0;
        total_cost = sub_project1.getProject_cost() +
                sub_project2.getProject_cost() +
                sub_project3.getProject_cost() +
                sub_project4.getProject_cost();
        ArrayList<Employer> employers = new ArrayList<>();
        Software_developer employer_with_type1 = new Software_developer("Kuat", 7);
        CS_analyst employer_with_type2 = new CS_analyst("Medet", 5);
        Web_developer employer_with_type3 = new Web_developer("Madi", 4);
        employers.add(new Software_developer(employer_with_type1.getName(), employer_with_type1.getExperience_years()));
        employers.add(new CS_analyst(employer_with_type2.getName(), employer_with_type2.getExperience_years()));
        employers.add(new Web_developer(employer_with_type3.getName(), employer_with_type3.getExperience_years()));
        main.insertEmployers(employers);

        try{
            Connection conn = connect();
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery("select * from employers where experience_years >= 3");
            System.out.println("Including tasks of the project: ");
            System.out.println(sub_project1.getProject_name());
            System.out.println(sub_project2.getProject_name());
            System.out.println(sub_project3.getProject_name());
            System.out.println(sub_project4.getProject_name());
            System.out.println("The total cost of the project: " + total_cost);
            System.out.println("The chosen employers for the project(Requirements: More than 3 years of experience!):");
            while(rs.next()){
                System.out.println(rs.getInt("id") + " "
                        + rs.getString("name") + " "
                        + rs.getInt("experience_years") + " "
                        + rs.getString("type"));
            }
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }

    }
}
