package kz.aitu.oop.practice_with_patterns.practice3;

public class Project {

    private String project_name;
    private double project_cost;

    public Project(ProjectBuilder projectBuilder){
        this.project_name = projectBuilder.project_name;
        this.project_cost = projectBuilder.project_cost;
    }

    public Project(String project_name, double project_cost) {
        this.project_name = project_name;
        this.project_cost = project_cost;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public double getProject_cost() {
        return project_cost;
    }

    public void setProject_cost(double project_cost) {
        this.project_cost = project_cost;
    }

}
