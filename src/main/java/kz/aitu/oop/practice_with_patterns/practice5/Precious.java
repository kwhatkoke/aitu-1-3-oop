package kz.aitu.oop.practice_with_patterns.practice5;

public class Precious extends Stone {

    private String stone_name;
    private String type;
    private int weight;
    private int cost;

    public Precious(PreciousBuilder preciousBuilder){
        this.type = "Precious";
        this.stone_name = preciousBuilder.name;
        this.weight = preciousBuilder.weight;
        this.cost = preciousBuilder.cost;
    }

    public Precious(String type, String stone_name, int weight, int cost) {
        this.type = type;
        this.stone_name = stone_name;
        this.weight = weight;
        this.cost = cost;
    }

    public String getType() {
        return type;
    }

    public String getStone_name() {
        return stone_name;
    }

    public int getWeight() {
        return weight;
    }

    public int getCost() {
        return cost;
    }
}
