package kz.aitu.oop.practice_with_patterns.practice5;


public class Stone {

    private String type;
    private String stone_name;
    private int weight;
    private int cost;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStone_name() {
        return stone_name;
    }

    public void setStone_name(String stone_name) {
        this.stone_name = stone_name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
