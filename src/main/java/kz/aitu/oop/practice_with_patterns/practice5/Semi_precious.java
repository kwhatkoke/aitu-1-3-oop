package kz.aitu.oop.practice_with_patterns.practice5;

public class Semi_precious extends Stone {

    private String stone_name;
    private String type;
    private int weight;
    private int cost;

    public Semi_precious(Semi_PreciousBuilder semi_preciousBuilder){
        this.type = "Semi-Precious";
        this.stone_name = semi_preciousBuilder.name;
        this.weight = semi_preciousBuilder.weight;
        this.cost = semi_preciousBuilder.cost;
    }

    public Semi_precious(String type, String stone_name, int weight, int cost) {
        this.type = type;
        this.stone_name = stone_name;
        this.weight = weight;
        this.cost = cost;
    }

    public String getType() {
        return type;
    }

    public String getStone_name() {
        return stone_name;
    }

    public int getWeight() {
        return weight;
    }

    public int getCost() {
        return cost;
    }
}
