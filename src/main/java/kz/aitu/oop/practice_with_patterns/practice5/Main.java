package kz.aitu.oop.practice_with_patterns.practice5;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final String url = "jdbc:mysql://localhost/practice?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String user = "root";
    private static final String password = "123456";

    public static Connection connect() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    public void insertStone(List<Stone> list) {
        String SQL = "INSERT INTO stones(type, stone_name, weight, cost) " + "VALUES(?,?,?,?)";
        try (
                Connection conn = connect();
                PreparedStatement statement = conn.prepareStatement(SQL);) {
            int count = 0;

            for (Stone stone : list) {
                statement.setString(1, stone.getType());
                statement.setString(2, stone.getStone_name());
                statement.setInt(3, stone.getWeight());
                statement.setInt(4, stone.getCost());
                statement.addBatch();
                count++;
                // execute every 100 rows or less
                if (count % 100 == 0 || count == list.size()) {
                    statement.executeBatch();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        ArrayList<Stone> stones = new ArrayList<>();
        PreciousBuilder preciousBuilder = new PreciousBuilder();
        Semi_PreciousBuilder semi_preciousBuilder = new Semi_PreciousBuilder();
        double total_cost = 0;
        double total_weight = 0;
        Precious precious1 = preciousBuilder.
                addPreciousName("Diamond").
                addPreciousCost(422000).
                addPreciousWeight(1).
                make();
        Precious precious2 = preciousBuilder.
                addPreciousName("Sapphire").
                addPreciousCost(625000).
                addPreciousWeight(2).
                make();
        Semi_precious semi_precious1 = semi_preciousBuilder.
                addSemiPreciousName("Agate").
                addSemiPreciousCost(60000).
                addSemiPreciousWeight(3).
                make();
        Semi_precious semi_precious2 = semi_preciousBuilder.
                addSemiPreciousName("Apatite").
                addSemiPreciousCost(90000).
                addSemiPreciousWeight(4).
                make();
        stones.add(new Precious(precious1.getType(), precious1.getStone_name(), precious1.getWeight(), precious1.getCost()));
        stones.add(new Precious(precious2.getType(), precious2.getStone_name(), precious2.getWeight(), precious2.getCost()));
        stones.add(new Semi_precious(semi_precious1.getType(), semi_precious1.getStone_name(), semi_precious1.getWeight(), semi_precious1.getCost()));
        stones.add(new Semi_precious(semi_precious2.getType(), semi_precious2.getStone_name(), semi_precious2.getWeight(), semi_precious2.getCost()));
        main.insertStone(stones);

        try{
            Connection conn = connect();
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery("select * from stones where type = 'Precious'");
            System.out.println("Selected for necklace(Only Precious): ");
            while(rs.next()){
                System.out.println(rs.getInt("id") + " "
                        + rs.getString("stone_name") + " "
                        + rs.getString("type") + " "
                        + rs.getDouble("weight") + " "
                        + rs.getDouble("cost"));
                total_cost += rs.getDouble("cost");
                total_weight += rs.getDouble("weight");
            }
            System.out.println("The total cost is: " + total_cost + " KZT");
            System.out.println("The total weight: " + total_weight + " carats");
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
