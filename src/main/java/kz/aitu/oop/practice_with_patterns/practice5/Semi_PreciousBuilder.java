package kz.aitu.oop.practice_with_patterns.practice5;

public class Semi_PreciousBuilder {

    public String name;
    public int weight;
    public int cost;

    public Semi_PreciousBuilder addSemiPreciousWeight(int weight){
        this.weight = weight;
        return this;
    }

    public Semi_PreciousBuilder addSemiPreciousName(String name){
        this.name = name;
        return this;
    }

    public Semi_PreciousBuilder addSemiPreciousCost(int cost){
        this.cost = cost;
        return this;
    }

    public Semi_precious make(){
        return new Semi_precious(this);
    }
}
