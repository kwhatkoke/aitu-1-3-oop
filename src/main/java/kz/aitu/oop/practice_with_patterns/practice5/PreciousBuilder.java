package kz.aitu.oop.practice_with_patterns.practice5;

public class PreciousBuilder {

    public String name;
    public int weight;
    public int cost;

    public PreciousBuilder addPreciousWeight(int weight){
        this.weight = weight;
        return this;
    }

    public PreciousBuilder addPreciousName(String name){
        this.name = name;
        return this;
    }

    public PreciousBuilder addPreciousCost(int cost){
        this.cost = cost;
        return this;
    }

    public Precious make(){
        return new Precious(this);
    }
}
