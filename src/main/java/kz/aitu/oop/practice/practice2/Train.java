package kz.aitu.oop.practice.practice2;


import java.util.LinkedList;

public class Train {
    private double capacity;
    private double total_capacity;

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public double total_capacity(int number_of_carrieges, double capacity){
        Carriege carriege = new Carriege(250);
        number_of_carrieges = carriege.getNumber_of_carrieges();
        return number_of_carrieges*capacity;
    }

    public double total_passengers(int passengers){
        LinkedList<Carriege> carrieges = new LinkedList<Carriege>();
        int total = 0;
        for(int i=0; i<carrieges.size(); i++){
            total += passengers;
        }
        return total;
    }
}
