package kz.aitu.oop.practice.practice2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) {

        Locomotive locomotive = new Locomotive(2);
        Train train = new Train();
            try{
                int total_capacity = 0;
                int total_passengers = 0;
                String url = "jdbc:mysql://localhost/practice?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
                Class.forName("com.mysql.cj.jdbc.Driver");
                Connection con= DriverManager.getConnection(url,"root","123456");
                Statement stmt=con.createStatement();
                ResultSet rs=stmt.executeQuery("select * from train");
                while(rs.next()){
                    int id = rs.getInt("id");
                    int capacity = rs.getInt("capacity");
                    int price = rs.getInt("price");
                    int passengers = rs.getInt("passengers");
                    System.out.println(id+" "+capacity+" "+price+" "+passengers);
                    total_capacity += capacity;
                    total_passengers += passengers;
                }
                System.out.println("Total capacity: "+total_capacity);
                System.out.println("Total amount of passengers: "+total_passengers);
                con.close();
            }catch(Exception e){
                e.printStackTrace();
            }
    }
}
