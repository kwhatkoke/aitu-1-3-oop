package kz.aitu.oop.practice.practice4;

public class Corydora extends Fish{

    private double price;
    private double size;
    private String name = "Corydora";

    public Corydora(double cost, double size) {
        super(cost, size);
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }
}
