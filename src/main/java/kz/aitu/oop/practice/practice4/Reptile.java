package kz.aitu.oop.practice.practice4;

public class Reptile extends Aquarium{
    Turtle turtle = new Turtle();
    private String name = turtle.getName();
    private String type = "Reptile";
    private double size;

    public Reptile(double cost, double size) {
        super(cost, size);
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
