package kz.aitu.oop.practice.practice4;

public class Fish extends Aquarium {

    Catfish catfish = new Catfish();
    private String name = catfish.getName();
    private String type = "Fish";

    public Fish(double cost, double size) {
        super(cost, size);
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public double getSize() {
        return super.getSize();
    }

    @Override
    public void setSize(double size) {
        super.setSize(size);
    }

    @Override
    public double getCost() {
        return super.getCost();
    }

    @Override
    public void setCost(double cost) {
        super.setCost(cost);
    }
}

