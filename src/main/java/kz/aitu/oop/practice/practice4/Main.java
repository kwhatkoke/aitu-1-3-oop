package kz.aitu.oop.practice.practice4;

import kz.aitu.oop.practice.practice3.Employer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final String url = "jdbc:mysql://localhost/practice?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String user = "root";
    private static final String password = "123456";

    public static Connection connect() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    public void insertToAquarium(List<Aquarium> list) {
        String SQL = "INSERT INTO aquarium(type, name, cost, size) " + "VALUES(?,?,?,?)";
        try (
                Connection conn = connect();
                PreparedStatement statement = conn.prepareStatement(SQL);) {
            int count = 0;

            for (Aquarium aquarium : list) {
                statement.setString(1, aquarium.getType());
                statement.setString(2, aquarium.getName());
                statement.setDouble(3, aquarium.getCost());
                statement.setDouble(4, aquarium.getSize());
                statement.addBatch();
                count++;
                // execute every 100 rows or less
                if (count % 100 == 0 || count == list.size()) {
                    statement.executeBatch();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        ArrayList<Aquarium> aquariums = new ArrayList<>();
        Fish fish1 = new Fish(5000, 10);
        Fish fish2 = new Fish(6000, 15);
        Reptile reptile1 = new Reptile(4500, 12);
        double total_cost = 0;
        aquariums.add(new Fish(fish1.getCost(), fish1.getSize()));
        aquariums.add(new Fish(fish2.getCost(), fish2.getSize()));
        aquariums.add(new Reptile(reptile1.getCost(), reptile1.getSize()));
        main.insertToAquarium(aquariums);

        try{
            Connection conn = connect();
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery("select * from aquarium");
            while(rs.next()){
                System.out.println(rs.getInt("id") + " "
                        + rs.getString("name") + " "
                        + rs.getInt("cost") + " "
                        + rs.getInt("size"));
                total_cost += rs.getInt("cost");
            }
            System.out.println("The total cost: " + total_cost);
            conn.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
