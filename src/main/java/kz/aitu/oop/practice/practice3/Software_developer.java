package kz.aitu.oop.practice.practice3;

public class Software_developer extends Employer{

    private String type = "Software developer";

    public Software_developer(String name, int experience_years) {
        super(name, experience_years);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getExperience_years() {
        return super.getExperience_years();
    }

    @Override
    public void setExperience_years(int experience_years) {
        super.setExperience_years(experience_years);
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }
}
