package kz.aitu.oop.practice.practice3;

public class CS_analyst extends Employer{

    private String type = "Computer System Analyst";

    public CS_analyst(String name, int experience_years) {
        super(name, experience_years);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public void setExperience_years(int experience_years) {
        super.setExperience_years(experience_years);
    }

    @Override
    public int getExperience_years() {
        return super.getExperience_years();
    }

    @Override
    public String getName() {
        return super.getName();
    }
}
