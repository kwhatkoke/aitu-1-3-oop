package kz.aitu.oop.practice.practice5;

import java.sql.*;

public class Stone {

    private static int total_weight = 0;
    private static int total_cost = 0;
    private static int id;
    private static String type;
    private static String stone_name;
    private static int weight;
    private static int cost;

    public static int getId() {
        return id;
    }

    public static int getTotal_cost() {
        return total_cost;
    }

    public static int getTotal_weight() {
        return total_weight;
    }

    public static int getCost() {
        return cost;
    }

    public static int getWeight() {
        return weight;
    }

    public static String getStone_name() {
        return stone_name;
    }

    public static void main(String[] args) {
        try{
            String url = "jdbc:mysql://localhost/practice?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con=DriverManager.getConnection(url,"root","123456");
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("select * from stones where type = 'Precious'");
            while(rs.next()){
                id = rs.getInt("id");
                stone_name = rs.getString("stone_name");
                type = rs.getString("type");
                weight = rs.getInt("weight");
                cost = rs.getInt("cost");
                System.out.println(id+" "+stone_name+" "+type+" "+weight+" "+cost);
                total_weight += weight;
                total_cost += cost;
            }
            System.out.println("Total weight of Precious stones: " + total_weight + " carats");
            System.out.println("Total cost of Precious stones: " + total_cost + " KZT");
            con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
