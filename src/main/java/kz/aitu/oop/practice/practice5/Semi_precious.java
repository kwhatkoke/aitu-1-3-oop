package kz.aitu.oop.practice.practice5;

public class Semi_precious extends Stone{

    private String type;

    public Semi_precious(){
        this.type = "Semi-precious";
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
