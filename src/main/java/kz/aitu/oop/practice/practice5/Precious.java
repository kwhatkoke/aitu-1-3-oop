package kz.aitu.oop.practice.practice5;

public class Precious extends Stone{

    private String type;

    public Precious(){
        this.type = "Precious";
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
