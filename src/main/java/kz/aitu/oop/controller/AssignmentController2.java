package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.examples.Point;
import kz.aitu.oop.repository.StudentFileRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/task/2")
public class AssignmentController2 {


    @GetMapping("/students")
    public ResponseEntity<?> getStudents() throws FileNotFoundException {

        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        for (Student student: studentFileRepository.getStudents()) {
            result += student.getGroup() + "\t" + student.getName() + "\t" + student.getAge() + "\t" + student.getPoint() + "</br>";
        }

        return ResponseEntity.ok(result);
    }

    /**
     * Method get all Students from file and calculate average name lengths
     * @return average name length of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsNameLength")
    public ResponseEntity<?> averageStudentsNameLength() throws FileNotFoundException {

        double average = 0;
        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getName().length();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }


    @GetMapping("/averageStudentsCount")
    public ResponseEntity<?> averageStudentsCount() throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double count = 0;
        double sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0, sum = 0;
        for (Student student: studentFileRepository.getStudents()) {
            if(student.getGroup().equals("IT1901")){
                sum1++;
            }
            if(student.getGroup().equals("IT1906")){
                sum2++;
            }
            if(student.getGroup().equals("IT1910")){
                sum3++;
            }
            if(student.getGroup().equals("IT1914")){
                sum4++;
            }
            sum = sum1 + sum2 + sum3 + sum4;
            count = sum / 4;

        }
        return ResponseEntity.ok(count);
    }

    @GetMapping("/averageStudentsPoint")
    public ResponseEntity<?> averageStudentsPoint() throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double average = 0;
        double count = 0;
        double points = 0;
        //change your code here
        for (Student student: studentFileRepository.getStudents()) {
             points += student.getPoint();
             count++;
        }
        average = points / count;
        return ResponseEntity.ok(average);
    }

    @GetMapping("/averageStudentsAge")
    public ResponseEntity<?> averageStudentsAge() throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double average = 0;
        double ages  = 0;
        double count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            ages += student.getAge();
            count++;
        }
        average = ages / count;
        return ResponseEntity.ok(average);
    }

    @GetMapping("/highestStudentsPoint")
    public ResponseEntity<?> highestStudentsPoint() throws FileNotFoundException {
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double maxPoint = 0;
        for (Student student: studentFileRepository.getStudents()) {
            if(maxPoint < student.getPoint()){
                maxPoint = student.getPoint();
            }
        }

        return ResponseEntity.ok(maxPoint);
    }

    @GetMapping("/highestStudentsAge")
    public ResponseEntity<?> highestStudentsAge() throws FileNotFoundException {
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double maxAge = 0;
        for (Student student: studentFileRepository.getStudents()) {
            if(maxAge < student.getAge()){
                maxAge = student.getAge();
            }
        }
        return ResponseEntity.ok(maxAge);
    }

    @GetMapping("/highestGroupAveragePoint")
    public ResponseEntity<?> highestGroupAveragePoint() throws FileNotFoundException {
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double averageGroupPoint = 0;
        double sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
        double ave1 = 0, ave2 = 0, ave3 = 0, ave4 = 0;
        double c1 = 0, c2 = 0, c3 = 0, c4 = 0;
        for (Student student: studentFileRepository.getStudents()) {
            if(student.getGroup().equals("IT1901")){
                sum1 += student.getPoint();
                c1++;
            }
            if(student.getGroup().equals("IT1906")){
                sum2 += student.getPoint();
                c2++;
            }
            if(student.getGroup().equals("IT1910")){
                sum3 += student.getPoint();
                c3++;
            }
            if(student.getGroup().equals("IT1914")){
                sum4 += student.getPoint();
                c4++;
            }
        }
        ave1 = sum1 / c1;
        ave2 = sum2 / c2;
        ave3 = sum3 / c3;
        ave4 = sum4 / c4;
        if(ave1>=ave2){
            if(ave1>=ave3){
                if(ave1>=ave4){
                    averageGroupPoint = ave1;
                }else{
                    averageGroupPoint = ave4;
                }
            }else{
                if(ave3>=ave4){
                    averageGroupPoint = ave3;
                }else{
                    averageGroupPoint = ave4;
                }
            }
        }else{
            if(ave2>=ave3){
                if(ave2>=ave4){
                    averageGroupPoint = ave2;
                }else{
                    averageGroupPoint = ave4;
                }
            }else{
                if(ave3>=ave4){
                    averageGroupPoint = ave3;
                }else{
                    averageGroupPoint = ave4;
                }
            }
        }
        return ResponseEntity.ok(averageGroupPoint);
    }

    @GetMapping("/highestGroupAverageAge")
    public ResponseEntity<?> highestGroupAverageAge() throws FileNotFoundException {
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double averageGroupAge = 0;
        double sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
        double ave1 = 0, ave2 = 0, ave3 = 0, ave4 = 0;
        double c1 = 0, c2 = 0, c3 = 0, c4 = 0;
        for (Student student: studentFileRepository.getStudents()) {
            if(student.getGroup().equals("IT1901")){
                sum1 += student.getAge();
                c1++;
            }
            if(student.getGroup().equals("IT1906")){
                sum2 += student.getAge();
                c2++;
            }
            if(student.getGroup().equals("IT1910")){
                sum3 += student.getAge();
                c3++;
            }
            if(student.getGroup().equals("IT1914")){
                sum4 += student.getAge();
                c4++;
            }
        }
        ave1 = sum1 / c1;
        ave2 = sum2 / c2;
        ave3 = sum3 / c3;
        ave4 = sum4 / c4;
        if(ave1>=ave2){
            if(ave1>=ave3){
                if(ave1>=ave4){
                    averageGroupAge = ave1;
                }else{
                    averageGroupAge = ave4;
                }
            }else{
                if(ave3>=ave4){
                    averageGroupAge = ave3;
                }else{
                    averageGroupAge = ave4;
                }
            }
        }else{
            if(ave2>=ave3){
                if(ave2>=ave4){
                    averageGroupAge = ave2;
                }else{
                    averageGroupAge = ave4;
                }
            }else{
                if(ave3>=ave4){
                    averageGroupAge = ave3;
                }else{
                    averageGroupAge = ave4;
                }
            }
        }

        return ResponseEntity.ok(averageGroupAge);
    }


    @GetMapping("/all")
    public ResponseEntity<?> allData() throws FileNotFoundException {

        String result = "";
        result += "averageStudentsNameLength: " + averageStudentsNameLength().getBody() + "</br>";
        result += "averageStudentsCount: " + averageStudentsCount().getBody() + "</br>";
        result += "averageStudentsPoint: " + averageStudentsPoint().getBody() + "</br>";
        result += "averageStudentsAge: " + averageStudentsAge().getBody() + "</br>";
        result += "highestStudentsPoint: " + highestStudentsPoint().getBody() + "</br>";
        result += "highestStudentsAge: " + highestStudentsAge().getBody() + "</br>";
        result += "highestGroupAveragePoint: " + highestGroupAveragePoint().getBody() + "</br>";
        result += "highestGroupAverageAge: " + highestGroupAverageAge().getBody() + "</br>";

        return ResponseEntity.ok(result);
    }
}
