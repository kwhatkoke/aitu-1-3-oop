package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

//import kz.aitu.oop.repository.StudentDBRepository;

@RestController
@RequestMapping("/api/task/4")
@AllArgsConstructor
public class AssignmentController4 {


    /**
     *
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {

        String result = "";
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            if (student.getGroup().equals(group)){
                result += student.getName() + "<br>";
            }
        }
        return ResponseEntity.ok(result);
    }

    /**
     *
     * @param group
     * @return stats by point letter (counting points): example  A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {

        String result = "";
        int A = 0, B = 0, C = 0, D = 0, F = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            if (student.getGroup().equals(group)){
                if(student.getPoint()>=90){
                    A++;
                }
                if(student.getPoint()<90 && student.getPoint()>=80){
                    B++;
                }
                if(student.getPoint()<80 && student.getPoint()>=70){
                    C++;
                }
                if(student.getPoint()<70 && student.getPoint()>=60){
                    D++;
                }
                if(student.getPoint()<60){
                    F++;
                }
            }
        }
        result += "A - " + A + ", " + "B - " + B + ", " + "C - " + C + ", " + "D - " + D + ", " + "F - " + F;

        return ResponseEntity.ok(result);
    }

    /**
     *
     * @return top 5 students name by point
     * @throws FileNotFoundException
     */
    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException {

        String result = "";
        List<String> student_name = new LinkedList<>();
        List<Double> student_point = new LinkedList<>();
        double max, max_point = 100;
        String top_student_name = "";
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        while (student_name.size()<5) {
            max = 0;
            for (Student student : studentFileRepository.getStudents()) {
                if (max < student.getPoint() && max_point > student.getPoint()){
                    top_student_name = student.getName();
                    max = student.getPoint();
                }
            }
            max_point = max;
            student_name.add(top_student_name);
            student_point.add(max);
        }
        int order = 1;
        for (int i = 0; i < 5; i++){
            result += "TOP" + order + ":" + student_name.get(i) + ", " + student_point.get(i) + "<br>";
            order++;
        }
        return ResponseEntity.ok(result);

    }
}
